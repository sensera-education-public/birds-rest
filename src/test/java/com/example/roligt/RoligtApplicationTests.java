package com.example.roligt;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.reactive.server.WebTestClient;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles("test")
class RoligtApplicationTests {

    @Autowired
    BirdsController birdsController;

    @Autowired
    BirdRepository birdRepository;

    @Autowired
    WebTestClient webTestClient;

    @BeforeEach
    void setUp() {
        birdRepository.saveAll(List.of(
                new BirdEntity("Påfågel"),
                new BirdEntity("Örn")
        ));
    }

    @AfterEach
    void tearDown() {
        birdRepository.deleteAll();
    }

    @Test
    void test() {
        List<Bird> birds = birdsController.all();

        assertEquals(2, birds.size());
    }

    @Test
    void name() {
        List<Bird> birds = webTestClient.get()
                .uri("/api/birds")
                .exchange()
                .returnResult(Bird.class)
                .getResponseBody()
                .collectList()
                .block();

        assertEquals(2, birds.size());
    }

    @Test
    void name2() {
        Bird kungsfågel = birdsController.create(new CreateBird("Kungsfågel"));

        assertEquals("Kungsfågel", kungsfågel.getName());
    }
}

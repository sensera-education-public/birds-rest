package com.example.roligt;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;


@Repository
public interface BirdRepository extends JpaRepository<BirdEntity, String> {

    @Modifying(flushAutomatically = true)
    @Query("update BirdEntity u set u.name = :birdName where u.id=:id")
    void updateBirdName(@Param("id") String id, @Param("birdName") String birdName);
}

package com.example.roligt;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Stream;

@Service
@AllArgsConstructor
public class BirdService {
    BirdRepository birdRepository;

    public Stream<BirdEntity> all() {
        return birdRepository.findAll()
                .stream();
    }

    public BirdEntity get(String id) {
        return birdRepository.findById(id)
                .orElse(null);
    }

    public BirdEntity createBird(String name) {
        BirdEntity birdEntity = new BirdEntity(name);
        return birdRepository.save(birdEntity);
    }

    @Transactional
    public BirdEntity setBirdName(String birdId, String birdName) {
        birdRepository.updateBirdName(birdId, birdName);
        return get(birdId);
    }
}

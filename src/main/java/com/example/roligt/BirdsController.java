package com.example.roligt;

import lombok.AllArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/birds")
@AllArgsConstructor
@Log4j2
@CrossOrigin("*")
public class BirdsController {

    BirdService birdService;

    @GetMapping
    public List<Bird> all() {
        log.trace("all");
        return birdService.all()
                .map(this::toDTO)
                .collect(Collectors.toList());
    }

    @GetMapping("/{id}")
    public Bird get(@PathVariable("id") String id) {
        return toDTO(birdService.get(id));
    }

    @PostMapping
    public Bird create(@RequestBody CreateBird createBird) {
        log.trace("create {}",createBird);
        return toDTO(birdService.createBird(createBird.getName()));
    }

    @PutMapping(value = "/{id}", consumes = MediaType.TEXT_PLAIN_VALUE)
    public Bird updateBirdName(@PathVariable("id") String id, @RequestBody String birdName) {
        return toDTO(birdService.setBirdName(id, birdName));
    }

    private Bird toDTO(BirdEntity birdEntity) {
        return new Bird(
                birdEntity.getId(),
                birdEntity.getName()
        );
    }
}

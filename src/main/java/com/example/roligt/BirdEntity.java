package com.example.roligt;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.UUID;

@Data
@Entity
@Table(name = "bird")
@NoArgsConstructor
public class BirdEntity {
    @Id String id;
    String name;

    public BirdEntity(String name) {
        this.id = UUID.randomUUID().toString();
        this.name = name;
    }
}

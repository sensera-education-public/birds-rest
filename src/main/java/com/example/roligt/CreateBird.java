package com.example.roligt;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Value;

@Value
public class CreateBird {
    String name;

    @JsonCreator
    public CreateBird(@JsonProperty("name") String name) {
        this.name = name;
    }
}
